let timeSecond = document.querySelector(".timer__second")
let timeMinut = document.querySelector(".timer__minut")
let startTime = document.querySelector("#start");
let pauseTime = document.querySelector("#pause");
let resetTime = document.querySelector("#reset");
let interval;

let second = window.sessionStorage.getItem("second")
let minut = window.sessionStorage.getItem("minut")
let counterSecond = second ? second : 0;
let counterMinut = minut ? minut : 0;

   timeMinut.textContent = counterMinut.toString().padStart(2, "0");
   timeSecond.textContent = counterSecond.toString().padStart(2, "0");

startTime.addEventListener("click", () => {
   clearInterval(interval)
   interval = setInterval(startTimer, 1000);
});
pauseTime.addEventListener("click", () => {
   clearInterval(interval)
   window.sessionStorage.setItem("second", counterSecond)
   window.sessionStorage.setItem("minut", counterMinut)
});
resetTime.addEventListener("click", () => {
   counterSecond = 0;
   counterMinut = 0;
   clearInterval(interval)

   counterMinut = counterMinut.toString().padStart(2, "0");

   timeMinut.textContent = counterMinut.toString().padStart(2, "0");
   timeSecond.textContent = counterSecond.toString().padStart(2, "0");

   window.sessionStorage.removeItem("second")
   window.sessionStorage.removeItem("minut")
});


function startTimer() {
   counterSecond++
   if (counterSecond === 60) {
      counterSecond = 0;
      counterMinut++;
   }
   counterMinut = counterMinut.toString().padStart(2, "0");
   timeMinut.textContent = counterMinut;
   counterSecond = counterSecond.toString().padStart(2, "0");
   
   timeSecond.textContent = counterSecond;
}

// toString.padStart(23, "0")
// 1- 9 = 01 - 00000000000000009000000